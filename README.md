# Lista de Tareas

Back end en php para una app de lista de tareas



## Testing

Tiene pruebas unitarias en phpunit.
La salida del coverage se aloja en ```\testCoverage``` . 

**Correr pruebas:** 

0. Navegar a la raíz del proyecto y correr: ```mkdir testCoverage```
1. Para correr las  pruebas: ```composer test```
2. Ver la salida en \testCoverage\index.html


## SonarQube

Token: `9b50dc502c517213ea49a06eaa7c6a75d792672d`

Iniciar server local: `/opt/sonarqube/bin/linux-x86-64/sonar.sh start`
Ver server local: [http://localhost:9000](http://localhost:9000) con (**login=admin, password=admin**).

Cargar el scan:
```[bash]
sonar-scanner \
  -Dsonar.projectKey=ListaDeTareas \
  -Dsonar.sources=. \
  -Dsonar.host.url=http://localhost:9000 \
  -Dsonar.login=9b50dc502c517213ea49a06eaa7c6a75d792672d
```
