<?php
/**
 * MIT License
 *
 * Copyright 2020 EstebanCureno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Domain\ListaDeTareas;

use App\Common\IComparator;
use App\Common\IToString;

class Titulo implements IToString, IComparator
{
    private $_titulo;

    private function __construct(string $titulo){
        $this->_titulo = $titulo;
    }

    public static function desdeString(string $titulo):Titulo{
        return new self($titulo);
    }

    public function toString(): string
    {
        return $this->_titulo;
    }

    public function equals($obj): bool
    {
        if(get_class($obj) != self::class){
            throw new \InvalidArgumentException("El argumento debe ser de tipo: " . self::class );
        }
        return $this->_titulo == $obj->_titulo;
    }
}
