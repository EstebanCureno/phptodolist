<?php
/**
 * MIT License
 *
 * Copyright 2020 EstebanCureno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Domain\ListaDeTareas;


use App\Common\IIdentification;

class ListaDeTareas
{
    private $_titulo;
    private $_descripcion;
    private $_id;

    private function __construct(IIdentification $id,Titulo $titulo){
        $this->_titulo = $titulo;
        $this->_descripcion = Descripcion::fromString("");
        $this->_id =$id;
    }

    public static function crearLista(IIdentification $id,Titulo $titulo):self{
        return new self($id,$titulo);
    }

    public function getTitulo():Titulo{
        return $this->_titulo;
    }

    public function setDescripcion(Descripcion $descripcion){
        $this->_descripcion = $descripcion;
    }

    public function getDescripcion():Descripcion{
        return $this->_descripcion;
    }

    public function getId():IIdentification{
        return $this->_id;
    }

}
