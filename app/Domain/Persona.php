<?php
/**
 * MIT License
 *
 * Copyright 2020 EstebanCureno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Domain;

use App\Common\Id;

/**
 * Class Persona
 * @package App\Domain
 */
class Persona
{
    /**
     * @var \App\Domain\Correo
     */
    private $_correo;
    /**
     * @var IIdentification
     */
    private $_id;
    /**
     * @var Nombre
     */
    private $_nombre;

    /**
     * Persona constructor.
     * @param $infoPersona
     */
    private function __construct($infoPersona)
    {
        $this->_correo = Correo::desdeString($infoPersona["correo"]);
        $this->_id     = Id::desdeString($infoPersona["id"]);
        $this->_nombre = Nombre::desdeString($infoPersona["nombre"]);
    }

    /**
     * Crea una persona desde un arreglo
     *
     * Metodo factoría para crear personas desde un arreglo.
     *
     * @param $infoPersona
     * @return Persona
     */
    public static function desdeArreglo($infoPersona):Persona{
        return new self($infoPersona);
    }
}
