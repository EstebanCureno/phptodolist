<?php
/**
 * MIT License
 *
 * Copyright 2020 EstebanCureno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Common;


/**
 *
 * Class IdNulo Representa un Id Nulo
 *
 * Esta clase representa un Identificador Nulo.
 *
 * @package App\Domain
 */
class IdNulo extends Id
{


    /**
     * Representa todos los identificadores nulos con un string
     */
    private const ID = "ID NULO";

    /**
     * IdNulo constructor.
     */
    private
    function __construct()
    {
    }

    /**
     * Crea un objeto IdNulo desde un string
     *
     * Metodo factoria que genera id nulos desde un string
     *
     * @param string $id
     * @return IIdentification
     */
    public static function desdeString(string $id):IIdentification{
        return new self();
    }

    /**
     * Representación string del objeto
     *
     * Devuelve una representacio string del objeto IdNulo
     *
     * @return string
     */
    public function toString(): string
    {
        return self::ID;
    }

    /**
     * Indica si el Identificador es nulo
     *
     * Devuelve true para todos los casos puesto que es nulo
     *
     * @return bool
     */
    public function isNull(): bool
    {
        return true;
    }

    public function equals($id): bool
    {
        if(get_class($id) != self::class){
            throw new \InvalidArgumentException("No es de tipo :" . self::class);
        }
        return true;
    }
}
