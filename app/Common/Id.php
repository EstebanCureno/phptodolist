<?php
/**
 * MIT License
 *
 * Copyright 2020 EstebanCureno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Common;


/**
 * Clase que representa un identificador
 * @author  EstebanCureno
 * @license MIT
 */
class Id implements IIdentification, IIsNull, IComparator
{
    /**
     * Almacena el Identificador
     *
     * @var string
     */
    private  $_id;

    /**
     * Id constructor.
     * @param string $id
     */
    private function __construct(string $id)
   {
       $this->_id = $id;
   }

    /**
     * Crea un identificador desde un string.
     *
     * @param string $id
     * @return IIdentification
     */
    public static function desdeString(string $id):IIdentification
    {
       return new self($id);
    }

    /**
     * Genera una representación string del Id
     *
     * @return string
     */
    public function toString(): string
   {
       return $this->_id;
   }

    /**
     * Indica si el Id es nulo
     *
     * @return bool
     */
    public function isNull(): bool
   {
       return false;
   }

    public function equals($id):bool{
        if(get_class($id) != self::class){
            throw new \InvalidArgumentException("No es de tipo: " . self::class);
        }
        return $this->_id == $id->_id;
    }
}
