<?php
/**
 * MIT License
 *
 * Copyright 2020 EstebanCureno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace Domain;


use PHPUnit\Framework\TestCase;
use App\Domain\ListaDeTareas\Titulo;

class TituloTest extends TestCase
{
    private const TEST_TITLE = "TEST_TITLE";
    private const TEST_ANOTHER_TITLE = "TEST_ANOTHER_TITLE";

    public function testDesdeString()
    {
        $this->assertInstanceOf(Titulo::class,Titulo::desdeString(self::TEST_TITLE));
    }

    public function testToString()
    {
        $titulo = Titulo::desdeString(self::TEST_TITLE);
        $this->assertEquals(self::TEST_TITLE,$titulo->toString());
    }

    public function testEquals()
    {
        $titulo = Titulo::desdeString(self::TEST_TITLE);
        $titulo2 = Titulo::desdeString(self::TEST_TITLE);
        $this->assertTrue($titulo->equals($titulo2));
    }
    public function testNotEquals()
    {
        $titulo = Titulo::desdeString(self::TEST_TITLE);
        $titulo2 = Titulo::desdeString(self::TEST_ANOTHER_TITLE);
        $this->assertNotTrue($titulo->equals($titulo2));
    }
    public function testMustThrowInvalidArgumentExceptionInEqualsMethod()
    {
        $this->expectException(\InvalidArgumentException::class);
        $titulo = Titulo::desdeString(self::TEST_TITLE);
        $titulo2 = json_decode('{"NotATituloObjectType":"object"}');
        $titulo->equals($titulo2);
    }

}
