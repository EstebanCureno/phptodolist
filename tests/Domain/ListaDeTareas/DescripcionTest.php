<?php
/**
 * MIT License
 *
 * Copyright 2020 EstebanCureno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace Domain\ListaDeTareas;


use PHPUnit\Framework\TestCase;
use App\Domain\ListaDeTareas\Descripcion;

class DescripcionTest extends TestCase
{
    private const TEST_DESCRIPCION = "MI DESCRIPCION";
    public function testFromString(){
        $descripcion = Descripcion::fromString(self::TEST_DESCRIPCION);
        $this->assertInstanceOf(Descripcion::class, $descripcion);
    }

    public function testToString(){
        $descripcion = Descripcion::fromString(self::TEST_DESCRIPCION);
        $this->assertEquals(self::TEST_DESCRIPCION, $descripcion->toString());
    }

    public function testEquals(){
        $descripcion1 = Descripcion::fromString(self::TEST_DESCRIPCION);
        $descripcion2 = Descripcion::fromString(self::TEST_DESCRIPCION);
        $this->asserttrue($descripcion1->equals($descripcion2));
    }

    public function testEqualsMustThrowException(){
        $descripcion1 = Descripcion::fromString(self::TEST_DESCRIPCION);
        $descripcion2 = json_decode('{"a":"a"}');
        $this->expectException(\InvalidArgumentException::class);
        $descripcion1->equals($descripcion2);
    }
}
