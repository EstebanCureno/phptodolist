<?php
/**
 * MIT License
 *
 * Copyright 2020 EstebanCureno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace Domain\ListaDeTareas;


use App\Common\Id;
use App\Domain\ListaDeTareas\Descripcion;
use App\Domain\ListaDeTareas\ListaDeTareas;
use App\Domain\ListaDeTareas\Titulo;
use PHPUnit\Framework\TestCase;

class ListaDeTareasTest extends TestCase
{
    private const TEST_TITLE = "TITUlO";
    private const TEST_DESCRIPTION = "TEST DESCRIPTION";
    private const TEST_ID = "TEST ID";

    public function testCrearLista(){
        $listaDeTareas = $this->_crearListaDeTareasConUnTituloYIdAutoGenerado(self::TEST_TITLE);
        $this->assertInstanceOf(ListaDeTareas::class,$listaDeTareas);
    }

    public function testTitleIsCorrectAfterCreation(){
        $listaDeTareas = $this->_crearListaDeTareasConUnTituloYIdAutoGenerado(self::TEST_TITLE);
        $titulo = Titulo::desdeString(self::TEST_TITLE);
        $areEqual = $titulo->equals($listaDeTareas->getTitulo());
        $this->assertTrue($areEqual);
    }


    public function testCanSetDescription(){
        $listaDeTareas = $this->_crearListaDeTareasConUnTituloYIdAutoGenerado(self::TEST_TITLE);
        $descripcion   = Descripcion::fromString(self::TEST_DESCRIPTION);
        $listaDeTareas->setDescripcion($descripcion);

        self::assertTrue($descripcion->equals($listaDeTareas->getDescripcion()));
    }

    public function testCanCreateFromIdAndTitle(){
        $listaDeTareas = $this->_crearListaDeTareasConUnTituloYIdAutoGenerado(self::TEST_TITLE);
        self::assertInstanceOf(ListaDeTareas::class,$listaDeTareas);
    }

    public function testCanGetIdentification(){
        $listaDeTareas = $this->_crearListaDeTareasConUnTituloYIdAutoGenerado(self::TEST_TITLE);
        $id = Id::desdeString(self::TEST_ID);
        $this->assertTrue($id->equals($listaDeTareas->getId()));
    }

    /*
    |----------------------------------------------------------------
    | Metodos Privados De Los Test
    |----------------------------------------------------------------
    |
    | Apartir de aquí solo utilería para los test de arriba.
    |
     */

    private function _crearListaDeTareasConUnTituloYIdAutoGenerado($titulo):ListaDeTareas{
        return ListaDeTareas::crearLista(Id::desdeString(self::TEST_ID),Titulo::desdeString($titulo));
    }


}
