<?php
/**
 * MIT License
 *
 * Copyright 2020 EstebanCureno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace Common;

use PHPUnit\Framework\TestCase;
use App\Common\Id;

class IdTest extends TestCase
{
    private const TEST_ID = "ID";
    private const TEST_ANOTHER_ID = "ANOTHER_ID";
    public function testCanCreateFromString(){
        $this->assertInstanceOf(Id::class,
                                        Id::desdeString(self::TEST_ID)
        );
    }
    public function testMetodoToString(){
        $this->assertEquals(self::TEST_ID,
                                    Id::desdeString(self::TEST_ID)->toString()
        );
    }
    public function testNoEsNulo(){
        $this->assertFalse(Id::desdeString(self::TEST_ID)->isNull());
    }
    public function testEquals(){
        $id_A = Id::desdeString(self::TEST_ID);
        $id_B = Id::desdeString(self::TEST_ID);
        $this->assertTrue($id_A->equals($id_B));
    }

    public function testNotEquals(){
        $id_A = Id::desdeString(self::TEST_ID);
        $id_B = Id::desdeString(self::TEST_ANOTHER_ID);
        $this->assertFalse($id_A->equals($id_B));
    }
    public function testMustThrowInvalidArgumentException(){
        $id_A = Id::desdeString(self::TEST_ID);
        $id_B = json_decode('{"notAn":"IdObject"}');
        $this->expectException(\InvalidArgumentException::class);
        $id_A->equals($id_B);
    }
}
