<?php
/**
 * MIT License
 *
 * Copyright 2020 EstebanCureno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace Common;

use PHPUnit\Framework\TestCase;

use App\Common\IdNulo;


class IdNuloTest extends TestCase
{
    public function testCreaIdNulos(){
        $this->assertInstanceOf(IdNulo::class,IdNulo::desdeString("Nulo"));
    }

    public function testImplementaMetodoIsNull(){
        $this->assertTrue(IdNulo::desdeString("Nulo")->isNull());
    }

    public function testDevuelveRepresentacionStringNull(){
        $id = IdNulo::desdeString("Nulo");
        $this->assertEquals("ID NULO",$id->toString());
    }

    public function testEquals(){
        $idA = IdNulo::desdeString("asd");
        $idB = IdNulo::desdeString("ccc");

        $this->assertTrue($idA->equals($idB));
    }
    public function testMustThrowInvalidArgumentException(){
        $idA = IdNulo::desdeString("asd");
        $idB = json_decode('{"NotAnIdNulo":""}');

        $this->expectException(\InvalidArgumentException::class);
        $idA->equals($idB);
    }
}
